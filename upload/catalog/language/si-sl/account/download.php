<?php
// Heading
$_['heading_title']     = 'ගිණුම ඩවුන්ලොඩ් කිරීම       Account Downloads';

// Text
$_['text_account']      = 'ගිණුම ';
$_['text_downloads']    = 'ඩවුන්ලොඩ්    කිරීම';
$_['text_empty']        = 'ඔබ මිට පෙර ඇණවුම ඩවුන්ලොඩ් කර නොමැත!';

// Column
$_['column_order_id']   = ' ඇණවුම අංකය  ID';
$_['column_name']       = ' නම';
$_['column_size']       = ' ප්‍රමාණ';
$_['column_date_added'] = ' දිනය එකතු කරන ලදී';