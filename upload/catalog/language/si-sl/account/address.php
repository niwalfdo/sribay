<?php
// Heading
$_['heading_title']      = 'ලිපින පොත ';

// Text
$_['text_account']       = 'ගිණුම';
$_['text_address_book']  = 'ලිපින පොතට පිවිසීම';
$_['text_address_add']   = 'ලිපින එකතු කිරීම';
$_['text_address_edit']  = 'ලිපින සංස්කරණය කිරීම';
$_['text_add']           = 'ඔබගේ  ලිපිනය සාර්ථකව එකතු කරන ලදී';
$_['text_edit']          = 'ඔබගේ  ලිපිනය සාර්ථකව යාවත්කාල කරන ලදී ';
$_['text_delete']        = 'ඔබගේ  ලිපිනය සාර්ථකව මකා දමන ලදී ';
$_['text_empty']         = 'ඔබගේ  ගිණුමෙ ලිපිනයන් නැත';

// Entry
$_['entry_firstname']    = 'පළමු නම ';
$_['entry_lastname']     = 'අවසාන නම ';
$_['entry_company']      = 'කොම්පැනිය';
$_['entry_address_1']    = 'ලිපින 1';
$_['entry_address_2']    = 'ලිපින 2';
$_['entry_postcode']     = 'තැපැල්  කේතය';
$_['entry_city']         = 'නගරය';
$_['entry_country']      = 'රට';
$_['entry_zone']         = 'ආගම / රජය    state';
$_['entry_default']      = 'පෙර ලිපිනය ලබා ගැනීම   Default Address';

// Error
$_['error_delete']       = 'අනතුරු ඇඟවීම: ඔබ සතුව අවම වශයෙන් එක ලිපිනයක්වත් තිබිය යුතුයි!';
$_['error_default']      = 'අනතුරු ඇඟවීම: ඔබට ඔබගේ මුලික ලිපිනය වෙනස් කළ නොහැක!';
$_['error_firstname']    = 'පළමු නම අකුරු 1 ත් 32 ත් අතර විය යුතුයි!';
$_['error_lastname']     = 'අවසාන නම අකුරු1 ත් 32 ත් අතර විය යුතුයි!';
$_['error_address_1']    = 'ලිපිනය අකුරු 3 ත් 128 ත් අතර විය යුතුයි!';
$_['error_postcode']     = 'තැපැල් අංකය / සංකෙතය අකුරු 2 ත් 10 ත් අතර විය යුතුයි!';
$_['error_city']         = 'නගරය අකුරු 2 ත් 128 ත් අතර විය යුතුයි !';
$_['error_country']      = 'කරුණාකර රටක් තොරන්න!';
$_['error_zone']         = 'කරුණාකර ආගම / රජය තොරන්න!';
$_['error_custom_field'] = '%s අවශ්‍ය විය    required!';