<?php
// Heading
$_['heading_title']   = 'ඔබගේ රහස්  මුරපදය අමතක වීම?';

// Text
$_['text_account']    = ' ගිණුම ';
$_['text_forgotten']  = ' රහස්  මුරපදය අමතක විය ';
$_['text_your_email'] = ' ඔබගේ විද්‍යුත් තැපෑල ';
$_['text_email']      = ' ඔබගේ ගිණුම හා සම්බන්ධ විද්‍යුත් තැපෑල  ඇතුලත් කරන්න . විද්‍යුත් තැපෑල හා සම්බන්ධ වීමට රහස් මුරපදය ඔබන්න Click submit to have a password reset link e-mailed to you.';
$_['text_success']    = ' තහවුරු  කිරීමෙ link එක සමග  විද්‍යුත් තැපෑලක් ඔබගේ ගිණුමට ඒවා ඇත    An email with a confirmation link has been sent your email address.';

// Entry
$_['entry_email']     = 'විද්‍යුත් තැපෑල ලිපිනය ';
$_['entry_password']  = 'නව රහස් මුරපදය ';
$_['entry_confirm']   = 'ස්ථීර කිරීම';

// Error
$_['error_email']     = ' අවවාදයයි /අනතුරු ඇඟවීම : ඔබ හා සම්බන්ධ සටහන්වල /වාර්ථාවල විද්‍යුත් තැපෑල සොයාගත නොහැක , කරුණාකර නැවත උත්සහ කරන්න   The E-Mail Address was not found in our records, please try again!';
$_['error_approved']  = ' අවවාදයයි : ඔබ ඇතුල් වීමට පෙර ඔබගේ ගිණුමට අනුමැතිය අවශ්‍යයි  Your account requires approval before you can login.';
$_['error_password']  = ' මුරපදය අකුරු 4 ත් 20 ත් අතර විය යුතුයි !';
$_['error_confirm']   = ' මුරපදය සහ මුරපදය තහවුරු කිරීම නොගැලපෙයි !';