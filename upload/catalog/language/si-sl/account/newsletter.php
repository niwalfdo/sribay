<?php
// Heading
$_['heading_title']    = ' පුවත් ලිපි සඳහා දායකත්වය ';

// Text
$_['text_account']     = ' ගිණුම ';
$_['text_newsletter']  = ' පුවත් ලිපි ';
$_['text_success']     = ' සාර්තකයි :  ඔබගේ පුවත් ලිපි දායකත්වය සාර්තකව යාවත්කාලීන කර ඇත !';

// Entry
$_['entry_newsletter'] = ' දායකත්වය දක්වන්න ';