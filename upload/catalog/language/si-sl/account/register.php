<?php
// Heading
$_['heading_title']        = ' ලියාපදිංචි ගිණුම ';

// Text
$_['text_account']         = ' ගිණුම ';
$_['text_register']        = ' ලියාපදිංචිය ';
$_['text_account_already'] = ' ඔබට දැනටමත් අප සමග ගිණුමක් තිබෙ නම් ,  කරුණාකර  එය ඇතුලත් කරන්න /  If you already have an account with us, please login at the <a href="%s">login page</a>.';
$_['text_your_details']    = ' ඔබගේ පුද්ගලික තොරතුරු ';
$_['text_newsletter']      = ' පුවත් ලිපි ';
$_['text_your_password']   = ' ඔබගේ  මුරපදය / රහස් පදය ';
$_['text_agree']           = ' මම කියවා  එකඟ  වුණි  / I have read and agree to the <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] = ' පාරිභෝගික කණ්ඩායම /  සන්සදය     Customer Group';
$_['entry_firstname']      = ' පළමු  නම ';
$_['entry_lastname']       = ' අවසාන නම ';
$_['entry_email']          = ' විද්‍යුත් තැපෑල ';
$_['entry_telephone']      = ' දුරකථනය ';
$_['entry_newsletter']     = ' දායකත්වය දක්වන්න ';
$_['entry_password']       = ' රහස් පදය  / මුරපදය ';
$_['entry_confirm']        = ' රහස් පදය  / මුරපදය  තහවුරු  කරන්න ';

// Error
$_['error_exists']         = ' අවවාදයයි : විද්‍යුත් තැපෑල ලිපිනය දැනටමත් ලියාපදිංචි වී ඇත !';
$_['error_firstname']      = ' පළමු නම අකුරු 1 ත් 32 ත් අතර විය යුතුයි !';
$_['error_lastname']       = ' අවසාන නම අකුරු 1 ත් 32 ත් අතර විය යුතුයි !';
$_['error_email']          = ' විද්‍යුත් තැපෑල ලිපිනය වලංගු නොවෙ    / E-Mail Address does not appear to be valid!';
$_['error_telephone']      = ' දුරකථනය  අංකය අකුරු 3 ත් 32 ත් අතර විය යුතුයි !';
$_['error_custom_field']   = '%s අවශ්‍යයයි !';
$_['error_password']       = ' රහස් පදය  / මුරපදය අකුරු 4 ත් 20 ත් අතර විය යුතුයි !';
$_['error_confirm']        = ' රහස් පදය  / මුරපදය තහවුරු  කිරීම රහස් පදය  / මුරපදය සමග නොගැලපෙයි !';
$_['error_agree']          = ' අවවාදයයි :  ඔබ එකඟ විය යුතුයි   /  You must agree to the %s!';