<?php
// Heading
$_['heading_title']  = ' ඔබගේ  මුරපදය / රහස් පදය  නැවත සැකසීම  / Reset your password';

// Text
$_['text_account']   = ' ගිණුම ';
$_['text_password']  = ' ඔබ භාවිතා කිරීමට කැමති අලුත් / නව මුරපදය / රහස් පදය ඇතුලත් කරන්න .';
$_['text_success']   = ' සාර්ථකයි :  ඔබගේ  මුරපදය / රහස් පදය සාර්ථකව යාවත්කාලීන විය .';

// Entry
$_['entry_password'] = ' මුරපදය / රහස් පදය ';
$_['entry_confirm']  = ' තහවුරු  කිරීම ';

// Error
$_['error_password'] = ' මුරපදය / රහස් පදය අකුරු 4 ත් 20 ත් අතර විය යුතුයි !';
$_['error_confirm']  = ' මුරපදය / රහස් පදය සහ මුරපදය / රහස් පදය තහවුරු  කිරීම නොගැලපෙ !';
$_['error_code']     = ' මුරපදය / රහස් පදය නැවත සැකසීමෙ කේතය අවලංගු වී හෝ  පෙර භාවිතා කර ඇත  / Password reset code is invalid or was used previously!';