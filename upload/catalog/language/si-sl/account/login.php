<?php
// Heading
$_['heading_title']                = 'ගිණුමට ඇතුලත්වීම ';

// Text
$_['text_account']                 = 'ගිණුම ';
$_['text_login']                   = ' ඇතුලත්වීම ';
$_['text_new_customer']            = ' නව පාරිභෝගිකයා /  ගණුදෙනුකරු  ';
$_['text_register']                = ' ගිණුම ලියාපදිංචි කිරීම ';
$_['text_register_account']        = ' ඔබ ගිණුමක් විවෘත කිරීමෙන් ඔබට ඉක්මනින් මිළ දී ගැනීම් කළ හැක ,ඔබට ඔබගේ ඇණවුම සම්බන්ධව යාවත්කාලීන විය හැකි අතර ඔබ පෙර කරන ලද ඇණවුම සාර්ථකව සිදුවුවාද යන්න සොයා බැලිය හැක By creating an account you will be able to shop faster, be up to date on an order\'s status, and keep track of the orders you have previously made.';
$_['text_returning_customer']      = ' පාරිභෝගිකයා /  ගණුදෙනුකරු  වෙත ආපසු / නැවත ලබාදීම ';
$_['text_i_am_returning_customer'] = ' මම ආපසු / නැවත ලබාදෙන පාරිභෝගිකයෙකු වෙමි /  ගණුදෙනුකරුවෙකු වෙමි  ';
$_['text_forgotten']               = ' මුරපදය අමතක වුණි ';

// Entry
$_['entry_email']                  = ' විද්‍යුත් තැපෑල ලිපිනය ';
$_['entry_password']               = ' මුරපදය /රහස් පදය ';

// Error
$_['error_login']                  = ' අවවාදයයි:  විද්‍යුත් තැපෑල හා මුරපදය / රහස් පදය නොගැලපෙයි .';
$_['error_attempts']               = ' අවවාදයයි:  ඔබට ලබා දී ඇති ඇතුලත්වීමෙ වාර/අවස්ථා ගණන  අවසානයි .Your account has exceeded allowed number of login attempts. කරුණාකර පැයකට පසු නැවත උත්සහ කරන්න Please try again in 1 hour.';
$_['error_approved']               = ' අවවාදයයි:  ඔබගේ ගිණුමට ඇතුලත්වීමට පෙර අනුමැතිය අවශ්‍යයයි .';