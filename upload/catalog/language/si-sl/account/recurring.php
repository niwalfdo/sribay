<?php
// Heading
$_['heading_title']                        = ' ගෙවිම් නැවත ලබාගැනීම ';

// Text
$_['text_account']                         = ' ගිණුම ';
$_['text_recurring']                       = ' ගෙවිම් නැවත ලබාගැනීමෙ තොරතුරු ';
$_['text_recurring_detail']                = ' ගෙවිම් නැවත ලබාගැනීමෙ විස්තරය ';
$_['text_order_recurring_id']              = ' ගෙවිම් නැවත ලබාගැනීමෙ හැඳුනුම් අංකය  / ID:';
$_['text_date_added']                      = ' එක් කරන ලැබූ දිනය :';
$_['text_status']                          = ' ඔබ පසුවන ස්ථානය / Status:';
$_['text_payment_method']                  = ' ගෙවීමෙ ක්‍රමය / Payment Method:';
$_['text_order_id']                        = ' ඇණවුම හඳුනා ගැනිමෙ අංකය /   ID:';
$_['text_product']                         = ' නිෂ්පාදනය :';
$_['text_quantity']                        = ' ප්‍රමාණය :';
$_['text_description']                     = ' විස්තරය ';
$_['text_reference']                       = ' යොමුව  /  Reference';
$_['text_transaction']                     = ' ගණුදෙනුව  / Transactions';
$_['text_status_1']                        = ' ක්‍රියාමාර්ගය / සම්බන්ධ කිරීම  Active';
$_['text_status_2']                        = ' සම්බන්ධය ක්‍රියා විරහිත කිරීම  / Inactive';
$_['text_status_3']                        = ' අහෝසි  කිරීම  / cancelled';
$_['text_status_4']                        = ' අත්හිටුවා ඇත   /  Suspended';
$_['text_status_5']                        = ' කල්ඉකුත් විය   /  Expired';
$_['text_status_6']                        = ' අපේක්ෂිතයි  /  Pending';
$_['text_transaction_date_added']          = ' නිර්මාණය කරන ලදි  / ගිණුම බැර කිරීමට සුදානම් කර ඇත   /  Created';
$_['text_transaction_payment']             = ' ගෙවිම්   / Payment';
$_['text_transaction_outstanding_payment'] = ' ගෙවිමට ඇති දින ඉකුත් වී ඇති ගෙවිම්  /  Outstanding payment';
$_['text_transaction_skipped']             = ' පැහැර හරින ලද ගෙවිම්  / Payment skipped';
$_['text_transaction_failed']              = ' අසාර්තක වු ගෙවිම්  / Payment failed';
$_['text_transaction_cancelled']           = ' අහෝසි  කර ඇත   /  Cancelled';
$_['text_transaction_suspended']           = ' අත්හිටුවා ඇත / Suspended';
$_['text_transaction_suspended_failed']    = ' අසාර්තක ගෙවීම් වලින් අත්හිටුවා ඇත /  Suspended from failed payment';
$_['text_transaction_outstanding_failed']  = ' ගෙවිමට ඇති දින ඉකුත් වී ඇති ගෙවිම් අසාර්තකයි / Outstanding payment failed';
$_['text_transaction_expired']             = ' කල්ඉකුත් විය   /  Expired';
$_['text_empty']                           = ' ගෙවිම් නැවත ලබාගැනීම නොමැත  /  No recurring payments found!';
$_['text_error']                           = ' ඔබ ඉල්ලුම් කරන ලද  ගෙවිම් නැවත ලබාගැනීම සොයා ගත නොහැක  / The recurring order you requested could not be found!';
$_['text_cancelled']                       = ' ගෙවිම් නැවත ලබාගැනීමෙ  ඉල්ලීම අහෝසි විය   /  Recurring payment has been cancelled';

// Column
$_['column_date_added']                    = ' එක් කරන ලැබූ දිනය / Date Added';
$_['column_type']                          = ' වර්ගය  / Type';
$_['column_amount']                        = ' ප්‍රමාණය  / Amount';
$_['column_status']                        = ' ඔබ පසුවන ස්ථානය  / Status';
$_['column_product']                       = ' නිෂ්පාදනය  / Product';
$_['column_order_recurring_id']            = ' ගෙවිම් නැවත ලබාගැනීමෙ හැඳුනුම් අංකය /  Recurring ID';

// Error
$_['error_not_cancelled']                  = ' දෝෂ  /  වැරදි   /  Error: %s';
$_['error_not_found']                      = ' ගෙවිම් නැවත ලබාගැනීමෙ  අසාර්තක  වී නැත  / Could not cancel recurring';

// Button
$_['button_return']                        = ' ආපසු  පිවිසීම  / Return';