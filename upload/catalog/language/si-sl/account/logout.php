<?php
// Heading
$_['heading_title'] = ' ගිණුමෙන්  ඉවත්වීම / ලොග්අවුට් වීම ';

// Text
$_['text_message']  = '<p> ඔබ ඔබගේ ගිණුමෙන් ඉවත්වී  ඇත .  පරිගණකයෙන් දැන් ඉවත්වීම  ආරක්ෂිතයි .</p><p>Your shopping cart has been saved, the items inside it will be restored whenever you log back into your account.</p>';
$_['text_account']  = ' ගිණුම ';
$_['text_logout']   = ' ඉවත්වීම ';