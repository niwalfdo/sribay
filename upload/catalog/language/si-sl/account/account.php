<?php
// Heading
$_['heading_title']       = 'මගේ ගිණුම';

// Text
$_['text_account']        = ' ගිණුම';
$_['text_my_account']     = 'මගේ ගිණුම';
$_['text_my_orders']      = 'මගේ ඇණවුම';
$_['text_my_affiliate']   = 'මා හා සම්බන්ධ ගිණුම';
$_['text_my_newsletter']  = 'පුවත් ලිපි';
$_['text_edit']           = 'ඔබගේ ගිණුම සංස්කරණය  කිරීම';
$_['text_password']       = 'ඔබගේ   රහස්  මුරපදය වෙනස් කිරීම';
$_['text_address']        = 'ඔබගේ  ලිපින යොමුව වෙනස් කිරීම';
$_['text_credit_card']    = 'තැන්පත් ණයපත් හැසිරව්ම';
$_['text_wishlist']       = 'ඔබගේ කැමති/ මුර ලැයිස්තුව වෙනස් කිරීම  Modify your wishlist';
$_['text_order']          = 'ඇණවුම් ඉතිහාසය බැලීම     View your order history';
$_['text_download']       = 'භාගත  කිරීම   Downloads';
$_['text_reward']         = 'ඔබගේ  පාරිතොෂිත ලකුණු       ශුභඑලය      Your Reward Points';
$_['text_return']         = 'ඔබගේ නැවත ඉල්ලීමි  බැලීම     View your return requests';
$_['text_transaction']    = 'ඔබගේ  ගනුදෙනු       Your Transactions';
$_['text_newsletter']     = 'පුවත් ලිපි සඳහා එකගවීම / එකගනොවීම    Subscribe / unsubscribe to newsletter';
$_['text_recurring']      = 'නැවත ගෙවීම්          Recurring payments';
$_['text_transactions']   = 'ගනුදෙනු      Transactions';
$_['text_affiliate_add']  = 'අනුබද්ධ ගිණුම සඳහා ලියාපදිංචි විම      Register for an affiliate account';
$_['text_affiliate_edit'] = 'ඔබගේ  අනුබද්ධිත තොරතුරු   වෙනස් කිරීම    Edit your affiliate information';
$_['text_tracking']       = 'පාරිභෝගික සබධතාව නිරීක්ෂණය   Custom Affiliate Tracking Code';