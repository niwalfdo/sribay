<?php
// Heading
$_['heading_title']         = ' ඇණවුම් ඉතිහාසය ';

// Text
$_['text_account']          = ' ගිණුම ';
$_['text_order']            = ' ඇණවුම් විස්තර ';
$_['text_order_detail']     = ' ඇණවුම්  විස්තර / තොරතුරු       Order Details';
$_['text_invoice_no']       = ' ඉන්වොයිස්  අංකය .:';
$_['text_order_id']         = ' ඇණවුම  හැඳුනුම්පත  / ID:';
$_['text_date_added']       = ' එකතු  කරන ලද දිනය :';
$_['text_shipping_address'] = ' නැව් ගතකළ යුතු ලිපිනය ';
$_['text_shipping_method']  = ' නැව් ගතකළ යුතු ක්‍රමය :';
$_['text_payment_address']  = ' ගෙවීමට අදාල ලිපිනය ';
$_['text_payment_method']   = ' ගෙවීමෙ ක්‍රමය :';
$_['text_comment']          = ' ඇණවුම සම්බන්ධ අදහස්  ';
$_['text_history']          = ' ඇණවුම් ඉතිහාසය ';
$_['text_success']          = ' සාර්ථකයි :  ඔබ එකතු  කරන ලදී   <a href="%s">%s</a> to your <a href="%s"> සවාරි  ට්‍රොලිිය </a>!';
$_['text_empty']            = ' ඔබ පෙර කිසිම ඇණවුම් කර නොමැත !';
$_['text_error']            = ' ඔබ කරන ලද ඇණවුම  සොයාගත නොහැක !';

// Column
$_['column_order_id']       = ' ඇණවුම්  හැඳුනුම්පත / ID';
$_['column_customer']       = ' පාරිභෝගිකයා /  ගණුදෙනුකරු ';
$_['column_product']        = ' නිෂ්පාදන නැත           No. of Products';
$_['column_name']           = ' නිෂ්පාදනයේ  නම ';
$_['column_model']          = ' ආකෘතිය / මෝස්තරය ';
$_['column_quantity']       = ' ප්‍රමාණය ';
$_['column_price']          = ' මිල ';
$_['column_total']          = ' මුළු එකතුව ';
$_['column_action']         = ' ක්‍රියාමාර්ගය  /   Action';
$_['column_date_added']     = ' එකතු කරන ලද දිනය ';
$_['column_status']         = ' ඔබ පසුවන ස්ථානය  /   Status';
$_['column_comment']        = ' අදහස් දැක්වීම ';

// Error
$_['error_reorder']         = '%s දැනට වාර්තා ගතවීම්  සිදු වී නොමැත /  is not currently available to be reordered.';