<?php
// Heading
$_['heading_title']             = 'ඔබ හා සම්බන්ධිත ගිණුමෙ  තොරතුරු   Your Affiliate Information';

// Text
$_['text_account']              = 'ගිණුම';
$_['text_affiliate']            = 'සම්බන්ධිත  /  Affiliate';
$_['text_my_affiliate']         = 'මාගේ සම්බන්ධිත ගිණුම       My Affiliate Account';
$_['text_payment']              = 'ගෙව්මෙ තොරතුරු  Payment Information';
$_['text_cheque']               = 'චෙක්පත්';
$_['text_paypal']               = 'PayPal ක්‍රමය';
$_['text_bank']                 = 'බැංකුව මගින් ගෙව්ම     Bank Transfer';
$_['text_success']              = 'සාර්ථකයි: ඔබගේ ගිණුම  සාර්ථකව යාවත්කාලීන කරන ලදී .';
$_['text_agree']                = 'මම කියවා එකඟ වුණි I have read and agree to the <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_company']             = 'කොම්පැනිය';
$_['entry_website']             = 'අන්තර්ජාල පිටුව Web Site';
$_['entry_tax']                 = 'බදු හැ දුනුම්පත      Tax ID';
$_['entry_payment']             = 'ගෙව්මෙ  ක්‍රමය';
$_['entry_cheque']              = 'චෙක්පත  ගෙවන්නාගේ නම       Cheque Payee Name';
$_['entry_paypal']              = 'PayPal විද්‍යුත් තැපෑල් ගිණුම';
$_['entry_bank_name']           = 'බැංකුවෙ නම';
$_['entry_bank_branch_number']  = 'ABA/BSB නොම්බරය  (ශාඛා අංකය   Branch Number)';
$_['entry_bank_swift_code']     = 'SWIFT කේතය';
$_['entry_bank_account_name']   = 'ගිණුමෙ නම ';
$_['entry_bank_account_number'] = 'ගිණුමෙ අංකය';

// Error
$_['error_agree']               = 'අනතුරු ඇඟවීම: ඔබ එකඟ විය යුතුයි %s!';
$_['error_cheque']              = 'චෙක්පත  ගෙවන්නාගේ නම අවශ්‍යයි       Cheque Payee Name required!';
$_['error_paypal']              = 'PayPal විද්‍යුත් තැපෑල් ලිපිනය බල සහිත ලෙස නොපෙනෙ  Email Address does not appear to be valid!';
$_['error_bank_account_name']   = ' ගිණුමෙ නම අවශ්‍යයි               Account Name required!';
$_['error_bank_account_number'] = ' ගිණුම් අංකය අවශ්‍යයි        Account Number required!';
$_['error_custom_field']        = '%s අවශ්‍යයි      required!';